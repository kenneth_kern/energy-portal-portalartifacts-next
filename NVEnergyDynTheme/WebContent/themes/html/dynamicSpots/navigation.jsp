<%@ page session="false" buffer="none" %> 
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="../includePortalTaglibs.jspf" %>

<%-- This file displays navigation for a given type --%>
<%-- Types:
				top: displays the children of level 0
				primary: displays the children of level 1
				secondary: displays the children of levels 2 and 3
 --%>

<%-- lazy load the selection path array --%>
<portal-core:lazy-set var="selectionPath" elExpression="wp.selectionModel.selectionPath"/>

<%-- true if hidden pages should be shown in the navigation --%>
<portal-core:lazy-set var="showHiddenPages" elExpression=="wp.publicRenderParam['{http://www.ibm.com/xmlns/prod/websphere/portal/publicparams}hiddenPages']" />

<%-- define the startLevel, endLevel, root CSS class and root accessibility label for each type of navigation --%>
<c:choose>
<c:when test="${param.type == 'primary'}">
	<c:set var="startLevel" value="1"/>
	<c:set var="endLevel" value="1"/>
	<c:set var="rootClass" value="wpthemePrimaryNav wpthemeLeft"/>
	<c:set var="rootLabel" value="Portal Application"/>
</c:when>
<c:when test="${param.type == 'secondary'}">
	<c:set var="startLevel" value="2"/>
	<c:set var="endLevel" value="3"/>
	<%-- if the selection path length is longer than 4, 
	display the last two levels in the selection path instead of levels 2 and 3.
	This ensures the currently selected page appears in the navigation --%>
	<c:set var="selectionPathLength" value="${fn:length(selectionPath)}" />
	<c:if test="${(selectionPathLength > startLevel + 1)}">
		<c:set var="startLevel" value="${selectionPathLength - 2}"/>	
		<c:set var="endLevel" value="${selectionPathLength}"/> 
	</c:if>
	<c:set var="rootClass" value="wpthemeSecondaryNav"/>
	<c:set var="rootLabel" value="Application"/>
</c:when>
<c:when test="${param.type == 'tertiary'}">
	<c:set var="startLevel" value="4"/>
	<c:set var="endLevel" value="4"/>
	<c:set var="rootClass" value="wpthemeTertiaryNav"/>
	<c:set var="rootLabel" value="Application Children"/>
</c:when>
<c:otherwise> <%-- top and default values --%>
	<c:set var="startLevel" value="0"/>
	<c:set var="endLevel" value="0"/>
	<c:set var="rootClass" value="wpthemeHeaderNav"/>
	<c:set var="rootLabel" value="Portal"/>
</c:otherwise>
</c:choose>

<%-- true if the user agent is a mobile device --%>
<portal-logic:if deviceClass="smartphone/tablet">
	<c:set var="isMobile" value="true"/>
</portal-logic:if>

<%-- loop through from startLevel to endLevel, outputing a navigation for each level --%>
<c:forEach var="n" items="${selectionPath}" varStatus="status" begin="${startLevel}" end="${endLevel}" step="1">
	<c:set var="curLevel" value="${startLevel + (status.count-1)}"/> 

	<%-- false if this level of navigation has no visible children --%>
	<c:set var="hasChildren" value="false"/>

<%-- print out navigation if the selection path is not empty and children exist --%>
<c:if test="${(fn:length(selectionPath) > curLevel) && wp.navigationModel.hasChildren[selectionPath[curLevel]]}">

	<%-- open the navigation containers --%>
	<div class="wpthemeNavContainer${status.count}">
		<nav class="${rootClass}" aria-label="${rootLabel}" role="navigation">

				<%-- loop through all children of the page at the given curLevel --%>
				<c:forEach var="node" items="${wp.navigationModel.children[selectionPath[curLevel]]}">
		
					<%-- display the page if it is not hidden OR hidden pages are being shown --%>
					<c:set var="isHiddenPage" value="${node.metadata['com.ibm.portal.Hidden'] || (isMobile && node.metadata['com.ibm.portal.mobile.Hidden'])}" />
					<c:if test="${!isHiddenPage || showHiddenPages}">

					<%-- a visible child has been found, print out the <ul> --%>
					<c:if test="${!hasChildren}"> 
						<ul class="wpthemeNavList">
						<c:set var="hasChildren" value="true"/>
					</c:if>

					<%-- set the CSS class to be placed on the page title anchor below --%>
					<c:set var="titleClass" value=""/>
					<%-- if the page has a draft in the current project, choose the wpthemeDraftPageText class --%>
					<c:if test="${node.projectID != null}"><c:set var="titleClass" value=" wpthemeDraftPageText"/></c:if>
					<%-- if the page is hidden, choose the wpthemeHiddenPageText class --%>
					<c:if test="${isHiddenPage}"><c:set var="titleClass" value=" wpthemeHiddenPageText"/></c:if>
					<%-- if the page is BOTH in the current project and hidden, choose the wpthemeHiddenDraftPageText class --%>
					<c:if test="${isHiddenPage && node.projectID != null}"><c:set var="titleClass" value=" wpthemeHiddenDraftPageText"/></c:if>

					<%-- print out the page and highlight it if it is in the selection path (the current page or an ancestor of the current page) --%>
					<li class="wpthemeNavListItem wpthemeLeft<c:if test="${wp.selectionModel[node] != null}"> wpthemeSelected</c:if>">

						<%-- output a link to the page --%>
						<a href="?uri=nm:oid:${wp.identification[node]}" class="wpthemeLeft${titleClass}">

							<%-- start page title markup --%>
							<span lang="${node.title.xmlLocale}" dir="${node.title.direction}"><%--
								
								<!-- print out the page title -->
								--%><c:out value="${node.title}"/><%--
								
								<!-- mark the page if it is currently selected for accessibility -->
								--%><c:if test="${wp.identification[wp.selectionModel.selected] == wp.identification[node]}"><span class="wpthemeAccess"><portal-fmt:text key="currently_selected" bundle="nls.commonTheme"/></span></c:if><%--

							--%></span><%-- end page title markup --%>
						</a>

						<%-- output the close icon for dynamic pages --%>
						<portal-dynamicui:closePage node="${node}">
							<a class="wpthemeClose wpthemeLeft" href="<%closePageURL.write(out);%>" aria-label="<portal-fmt:text key="theme.close" bundle="nls.commonUI"/>">&#10799;</a>
						</portal-dynamicui:closePage>

					</li>

				</c:if>
				</c:forEach>

			<%-- close the navigation containers --%>
			<c:if test="${hasChildren}"> 
				</ul>
			</c:if>

		</nav>
		<div class="wpthemeClear"></div>
	</div>

</c:if>
</c:forEach>
